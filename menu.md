# Sposoby generowania menu

## Na podstawie nagłówków dokumentu MD

Zastosowanie w przypadku jednoplikowej dokumentacji.

Tworzone jest wielopoziomowe menu na podstawie nagłówków w danym pliku MD.  
Poziomy menu są tworzone na podstawie poziomów nagłówków. Możliwe jest utworzenie 6 poziomowego menu (h1 - h6)

Kolejny poziom menu jest dodawany, gdy następny nagłówek jest **wyższego** poziomu niż poprzedni.  
Analogicznie wracamy do poprzedniego poziomu menu, gdy następny nagłówek jest **niższego** poziomu niż poprzedni.

Przykład:
 - w pliku MD
    ```markdown
    # Element 1
    ## Element 1.2
    # Element 2
    ### Element 2.1
    ## Element 2.2
    ### Element 2.2.1
    # Element 3
    ```
 - struktura wygenerowanego menu  
    ```
    Element 1
    - Element 2
    Element 2
    - Element 2.1
    - Element 2.2
    -- Element 2.2.1
    Element 3
    ```
 
 
## Na podstawie pliku (WIP)

Zastosowanie w przypadku wieloplikowej dokumentacji.  
Należy utworzyć dokument MD, w którym rozpisane zostaną wszystkie pliki dokumentacji - coś na wzór spisu treści. 
 
*Funkcjonalność w trakcie opracowywania. Szczegóły zostaną opisane później.*

## Rozwiązanie hybrydowe (WIP)

Połączenie [menu wygenerowanego z pliku](#na-podstawie-pliku) (menu główne) i [menu na podstawie nagłówków dokumentu MD](#na-podstawie-nagwkw-dokumentu-md) (menu uzupełniające)  
Menu uzupełniające jest dodawane jako podmenu do dopowiedniego elementu w menu głównym.   

*Funkcjonalność w trakcie opracowywania. Szczegóły zostaną opisane później.*  

## Konfiguracja ręczna

Menu tworzone indywidualnie w miejscu publikacji dokumentacji (np. w SN CMS)