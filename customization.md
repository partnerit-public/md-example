# Personalizacja układu i stylów

Personalizacji układu i stylów dokumentacji dokonujemy w szablonie przypisanym do typu dokumentu "MD doc".

## Elementy szablonu
 1. Generowanie HTMLa z plików MD
 2. Generowanie menu    
 3. Układ i style dokumentacji
  
 Każda z części powinna być być oznaczona stosownym komentarzem
 
## 1. Generowanie HTMLa z plików MD
*W trakcie dopracowywania. Dokładny opis zostanie przygotowany w późniejszym czasie.*

## 2. Generowanie menu
*W trakcie dopracowywania. Dokładny opis zostanie przygotowany w późniejszym czasie.*

## 3. Układ i style dokumentacji
W tej części można zamieścić dowolne treści HTML, CSS i JS - wszystko co można zrobbić w standardowym szablonie CMSa.  
Treści dokumentacji dostępne są w zmiennej `$html` - należy je osadzić w odpowiednim miejscu szablonu.
```php
<?= $html ?>
```
Elementy menu dostępne są w zmiennej `$menuItems` - należy je przekazać do mechanizmu renderującego menu. 
 Np. do vue-menu.