# Wstęp

Dokumentacje tworzymy z wykorzystaniem [GitHub Flavored Markdown (GFM)](https://guides.github.com/features/mastering-markdown/).
Do wykorzystania są wszystkie funkcjonalności jakie udostępnia GFM, z wykluczeniem tych które wymagają dodatkowych zależności (część z nich działa tylko w repozytoriach GitHub).

Dodatkową funkcją niezawartą w GFM jest załączanie plików MD w innchy pikach MD ([przykład](#jako-osobne-pliki-includowanie)).

 - [Załączanie fragmentów kodów](code-snippets.md)
 - [Załączanie grafik](images.md)
 - [Menu dokumentacji](menu.md)