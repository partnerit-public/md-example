# Załączanie grafik

Zaleca się umieszczanie grafik w katalogu `images`

Przykład:
 - wywolanie w dokumencie MD:
    ```markdown
    ![Przykładowa grafika](images/koala.jpg)
    ```
 - rezultat  
    ![Przykładowa grafika](images/koala.jpg)
