# Fragmenty kodu

## W bieżącym pliku MD

Jest to standardowa funkcjonalność języka Markdown

```javascript
alert("przykład");
```

```css
.przyklad{
    width: 100px;
    text-align: center;
}
```

```html
<div>
    <h1>Przykład</h1>
    <p>przykład paragraf</p>
</div>
```

## Jako osobne pliki (includowanie)

Zaleca się umieszczanie fragmentów kodu w katalogu `codes`.

Przykład:
 - sposób wywołania w pliku MD  
    ```
    @[Przykład js](codes/js-example.md) 
    ```   
 - zawartość pliku z fragmentem (`codes/js-example.md`)  
    ![Przykład fragmentu kodu js](images/md-code-snippet-example.png)
 - rezultat  
    @[Przykład js](codes/js-example.md) 
