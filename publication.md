# Publikacja dokumentacji w SN CMS

## Wymagania
 - link do repozytorium GIT, w którym znajduje się dokumentacja
    - ewentualny login i hasło uprawniający do czytania repozytorium
 - dostęp do CMS i uprawnienia do tworzenia dokumentów typu "MD doc"   

## Publikacja
W celu opublikowania dokumentacji należy wykonać poniższe kroki:
 1. W CMS tworzym nowy dokument typu MD doc  
    ![Utworzenie dokumentu typu MD doc](images/md-doc-document-creation.png)
 2. Uzupełniamy pola dokumentu - konfigurację dokumentacji
    - Pole "Url repozytorium GIT"   
      Tutaj podajemy adres url do repozytorium z dokumentacja MD.  
      W przypadku gdy dostęp do repozytorium wymaga autoryzacji, login i hasło należy zawrzeć w urlu.  
      np. `https://fake_login:secret_password@gitlab.com/partnerit-public/md-example.git`
    - Pole "Główny plik dokumentacji (entry point)"
      Podajemy ścieżkę do głównego pliku dokumentacji. Ścieżka powinna być ścieżką względną, względem głównego katalogu 
      repozytorium.
    - Pole "Miejsce przechowywania kopii repozytorium"
      Podajemy ścieżkę w Asset Manager (AM), w której zostanie umieszczona kopia dokumentacji. Ścieżka powinna być 
      ścieżką względną, względem głównego katalogu AM. W tym miejscu zostanie umieszczona tylko kopia katalogu 
      skonfigurowanego w polu "Katalog główny dokumentacji"
    - Pole "Referencja commita w repozytorium GIT"
      Podajemy tag, nazwę brancha lub ID commita. W ten sposób wskazujemy wersję, która zostanie opublikowana.
    - Pole "Katalog główny dokumentacji"
      Główny katalog dokumentacji. Podajemy ścieżkę względną, względem głównego katalogu repozytorium. Skonfigurowanie 
      tego parametru pozwala na niepublikowanie innych plików znajdujących sie w repozytorium, nie będących dokumentacją
    - Template (szablon)
      W chwili obecnej powinien być tylko jeden - domyślny. Więcej o szablonach w [Personalizacja układu i stylów](customization.md)      
    ![Konfiguracja dokumentacji](images/md-doc-config.png)
 3. Publikujemy dokumentację klikając w przycisk "Publish"
 4. Klikamy w link, pod którym została upubliczniona dokumentacja
     - Przy pierwszym wejściu dokumentacja zostanie pobrana z repozytorium GIT. Czas pobierania jest uzależniony od 
       wielkości całego repozytorium  