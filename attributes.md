{.class}
# Dodawanie atrybutów poprzez Markdown - przykłady

## Class 

### 1. Przykład

``` Markdown
{.custom-paragraph}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
```
Rezultat:

{.custom-paragraph}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

``` html
<p class="custom-paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
```

### 2. Przykład

``` Markdown
{.custom-header}
# Nagłówek
```
Rezultat:

{.custom-header}
# Nagłówek

``` html
<h1 class="custom-header" id="naglowek">Nagłówek</h1>
```

## ID 

### 1. Przykład

``` Markdown
{#lorem-ipsum}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
```
Rezultat:

{#lorem-ipsum}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

``` html
<p id="lorem-ipsum">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
```

### 2. Przykład

``` Markdown
{#header-1}
# Nagłówek
```
Rezultat:

{#header-1}
# Nagłówek

``` html
<h1 id="naglowek">Nagłówek</h1>
```

{.text-danger}
W tym przypadku nie zadziałał atrybut `{#header-1}` ponieważ istnieje konflikt z naszym generatorem IDków dla nagłówków


## Inne atrybuty

### Title

``` Markdown
{title="Blockquote title"}
> test
```
Rezultat:

{title="Blockquote title"}
> test

``` html
<blockquote title="Blockquote title">
    <p>test</p>
</blockquote>
```

### Style

``` Markdown
To jest *czerwony*{style="color: red"} tekst.
```
Rezultat:

To jest *czerwony*{style="color: red"} tekst.

``` html
<p>To jest <em style="color: red">czerwony</em> tekst.</p>
```

### Inne
Dowolny atrybut można dodać poprzez zapis `{<nazwa_atrybutu>=<wartosc_atrybutu>}` poprzedzający lub dodany bezpośrednio za elementem do którego chcemy dodać dany atrybut.

## Przykładowe zastosowanie

### Alerty

``` Markdown
{.alert .alert-primary}
Treść alertu

{.alert .alert-secondary}
Treść alertu

{.alert .alert-success}
Treść alertu

{.alert .alert-danger}
Treść alertu

{.alert .alert-warning}
Treść alertu

{.alert .alert-info}
Treść alertu

{.alert .alert-light}
Treść alertu

{.alert .alert-dark}
Treść alertu
```
Rezultat:

{.alert .alert-primary}
Treść alertu

{.alert .alert-secondary}
Treść alertu

{.alert .alert-success}
Treść alertu

{.alert .alert-danger}
Treść alertu

{.alert .alert-warning}
Treść alertu

{.alert .alert-info}
Treść alertu

{.alert .alert-light}
Treść alertu

{.alert .alert-dark}
Treść alertu

``` html
<p class="alert alert-primary">Treść alertu</p>

<p class="alert alert-secondary">Treść alertu</p>

<p class="alert alert-success">Treść alertu</p>

<p class="alert alert-danger">Treść alertu</p>

<p class="alert alert-warning">Treść alertu</p>

<p class="alert alert-info">Treść alertu</p>

<p class="alert alert-light">Treść alertu</p>

<p class="alert alert-dark">Treść alertu</p>
```

Alerty Bootstrap: https://getbootstrap.com/docs/4.0/components/alerts/

### Kolory tekstu

``` Markdown
{.text-primary}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-secondary}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-success}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-danger}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-warning}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-info}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-light .bg-dark}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-dark}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-muted}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-white .bg-dark}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
```

{.h4}
Rezultat:

{.text-primary}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-secondary}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-success}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-danger}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-warning}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-info}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-light .bg-dark}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-dark}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-muted}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{.text-white .bg-dark}
Lorem ipsum dolor sit amet, consectetur adipiscing elit.

``` html
<p class="text-primary">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

<p class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

<p class="text-success">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

<p class="text-danger">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

<p class="text-warning">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

<p class="text-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

<p class="text-light bg-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

<p class="text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

<p class="text-white bg-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
```
Kolory tekstów Bootstrap: https://getbootstrap.com/docs/4.0/utilities/colors/#color
